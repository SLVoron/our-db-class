<?php
$config = [
      'sqlengine' => 'mysql',
      'host' => 'localhost',
      'user' => 'root',
      'password' => '',
      'database' => 'personalhomepage',
];



class DataBase {
  private $pdo;
  public function __construct($config = null) {
    $this->connect($config['sqlengine'],$config['host'],$config['database'],$config['user'],$config['password']);
  }
  public function query($sql = '') {
    return $this->pdo->query($sql)/*->fetchAll(PDO::FETCH_ASSOC)*/;
  }
  private function connect($sqlengine, $host, $database, $user , $password) {
    try {
      $this->pdo = new PDO($sqlengine.':host='.$host.';dbname='.$database.';charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                         PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                         PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
    } catch (PDOException $e) {
        die('Подключение не удалось: ' . $e->getMessage());
    }
  }
  public function CloseConnection(){
    $this->pdo = null;
  }
}
$db = new DataBase($config);
?>